<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::get('/', 'MainController@index')->name('index');
Route::get('project_details/{id}', 'ProjectController@project_details')->name('project_details');
Route::get('about', 'AboutController@about')->name('about');
Route::get('about/{id}','AboutController@singleteam')->name('singleteam');
Route::get('projects', 'ProjectController@projects')->name('projects');
Route::get('/services', 'ServiceController@services')->name('services');
Route::get('/service/{id}', 'ServiceController@singleservice')->name('single.service');
Route::get('/service/{type}/{id}', 'ServiceController@subService')->name('single.service-type');
Route::get('/cost','CostController@cost')->name('cost');
Route::get('contact', 'MainController@contact')->name('contact');
Route::post('/contact/send', 'MainController@send')->name('contact.send');
