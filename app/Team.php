<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public $table = 'team';

    public function getTeam()
    {
        $team = DB::table('team')->get();
        return $team;
    }
    //
}
