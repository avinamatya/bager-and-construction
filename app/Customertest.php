<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customertest extends Model
{
    protected $fillable = ["title","image"];
}
