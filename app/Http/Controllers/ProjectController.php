<?php

namespace App\Http\Controllers;

use App\BadgerInfo;
use App\Project;
use App\Projectbanner;
use App\Projectlist;
use App\Service;
use App\Servicetype;
use App\Team;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    //
    public function __construct()
    {
        $this->info = new BadgerInfo();
        $this->projects = new Project();
        $this->projectlist = new Projectlist();

    }

    public function projects()
    {
        $info = $this->info->getInfo();
        $projects = $this->projects->with('projectlist')->get();
        $projectlist = $this->projectlist->all();
        $projectbanner = Projectbanner::all();
        $data = array(
            "info" => $info,
            "projects" => $projects,
            "projectlist" => $projectlist,
            "projectbanner"=>$projectbanner
        );
        return view('projects')->with("data", $data);
    }
    
    public function project_details($id)
    {
        $projectdetail = $this->projects->findorfail($id);
        $projectdetail->load("projectlist");

        return view('project_details', compact('projectdetail'));
    }

}
