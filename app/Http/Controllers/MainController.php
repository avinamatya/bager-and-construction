<?php

namespace App\Http\Controllers;
use App\About;
use App\Customer;
use App\Customertest;
use App\Extra;
use App\Hire;
use App\Mail\OrderShipped;
use Illuminate\Http\Request;
use App\BadgerInfo;
use App\Team;
use App\Service;
use Illuminate\Support\Facades\Mail;


class MainController extends Controller
{
    public function __construct()
    {
        $this->info = new BadgerInfo();
        $this->team = new Team();
        $this->services = new Service();
    }

    //
    public function index()
    {
        $info = $this->info->getInfo();
        $service = $this->services->getServices();
        $about = About::all();
        $customer = Customer::all();
        $extra = Extra::all();
        $hire = Hire::all();
        $testinomal = Customertest::all();
        $data = array(
            "info" => $info,
            "about" => $about,
            "customer" => $customer,
            "hire" => $hire,
            "extra" => $extra,
            "services"=>$service,
            "testinomal"=>$testinomal

        );
        return view('index')->with("data", $data);
    }





    public function contact()
    {
        $info = $this->info->getInfo();
        $data = array(
            "info" => $info
        );
        return view('contact')->with("data", $data);
    }

    public function send(Request $request)
    {
        $info = $this->info->getInfo();
        $data = array(
            "info" => $info
        );
        $order = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message
        );
        Mail::to($order['email'])->send(new OrderShipped(($order)));
        return view('contact')->with("data", $data);
    }

}


