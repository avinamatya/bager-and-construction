<?php

namespace App\Http\Controllers;

use App\BadgerInfo;
use App\Project;
use App\Projectlist;
use App\Service;
use App\Servicebanner;
use App\Servicetype;
use App\Team;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    //
    public function __construct()
    {
        $this->info = new BadgerInfo();
        $this->services = new Service();
        $this->services2 = new Servicetype();
    }

    public function services()
    {
        $info = $this->info->getInfo();
        $services = $this->services->getServices();
        $servicebanner = Servicebanner::all();
        $data = array(
            "info" => $info,
            "services" => $services,
            "servicebanner"=>$servicebanner
        );
        return view('services')->with("data", $data);
    }
    public function singleservice($id)
    {
        $info = $this->info->getInfo();
        $servicetype = Servicetype::with('service')->get();
        $services = Service::with('servicetype')->find($id);
        $allServices = Service::all();
        $data = array(
            "info" => $info,
            "services" => $services,
            "servicetype" => $servicetype,
            "allServices" => $allServices
        );

        return view('single_service')->with("data", $data)->with('id', $id);
    }

    public function subService($type, $id)
    {
        $info = $this->info->getInfo();
        $serviceType = Service::findOrFail($type);
        $innerService = $serviceType->servicetype()->findOrFail($id);
        $allServices = Service::all();
        $data = array(
            "info" => $info,
            "services" => $serviceType,
            "currentService" => $innerService,
            "allServices" => $allServices
        );


        return view('single_inside_service')->with("data", $data)->with('id', $type)->with('type', $id);
    }

}
