<?php

namespace App\Http\Controllers;

use App\About;
use App\Aboutbanner;
use App\BadgerInfo;
use App\Team;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    //
    public function __construct()
    {
        $this->info = new BadgerInfo();
        $this->team = new Team();
    }

    public function about()
    {
        $team = $this->team->getTeam();
        $info = $this->info->getInfo();

        $about = About::all();
        $aboutbanner = Aboutbanner::all();
        $data = array(
            "info" => $info,
            "team" => $team,
            "about" => $about,
            "aboutbanner"=>$aboutbanner

        );
        return view('about')->with("data", $data);
    }

    public function singleteam($id)
    {
        $team = Team::findorfail($id);
        return view('singleteam', compact('team'));
    }
}
