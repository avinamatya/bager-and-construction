<?php

namespace App\Http\Controllers;

use App\BadgerInfo;
use Illuminate\Http\Request;

class CostController extends Controller
{
    //
    public function __construct()
    {
        $this->info = new BadgerInfo();

    }

    public function cost(){
        $info = $this->info->getInfo();
        $data = array(
            "info" => $info
        );
        return view('cost')->with("data", $data);
    }
}
