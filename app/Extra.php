<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extra extends Model
{
    //
    protected $fillable=['title','description','delay1','delay2','icon'];
}
