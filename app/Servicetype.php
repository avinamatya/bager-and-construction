<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicetype extends Model
{
    //
    protected $fillable=['title','description','image','image2','service_id'];

    public function service(){
        return $this->belongsTo(Service::class,'service_id');
    }

}
