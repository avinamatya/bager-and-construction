<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicebanner extends Model
{
    //
    protected $fillable = ['title','image'];
}
