<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class BadgerInfo extends Model
{
    public $table = 'badger_info';
    public function getInfo()
    {
        $info = DB::table('badger_info')->first();
        return $info;
    }

    //
}
