<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Projectlist extends Model
{
    //
  public $table = "projectlists";
  public function projectlist(){
      $projectlist = DB::table('projectlists')->get();
      return $projectlist;
  }
  public function project(){
      return $this->hasMany(Project::class,'projectlist_id','id');
  }
}
