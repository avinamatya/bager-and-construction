<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projectbanner extends Model
{
    //
    protected $fillable =['title','image'];
}
