<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Project extends Model
{
    //
    public $table = 'projects';

    protected  $guarded = [];

    // protected $casts = [
    //     "date" => "datetime"
    // ];

    public function project()
    {
       return $this->get();
    }

    public function projectlist()
    {
        return $this->belongsTo(Projectlist::class);
    }


    public function getFormattedDateAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['date'])->format(" d M Y");
    
    }
}
