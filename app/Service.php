<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Service extends Model
{
    public $table = 'services';
    public function getServices(){
        $services = DB::table('services')->get();
        return $services;
    }
    public function servicetype(){
        return $this->hasMany(Servicetype::class,'service_id','id');
    }

}
