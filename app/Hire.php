<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hire extends Model
{
    //
    protected $fillable=['title1','title2','description','image'];
}
