@extends('layouts.layout')
@section('content')
<!-- subheader -->
<section id="subheader" data-speed="8" data-type="background">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Contact</h1>
                <ul class="crumb">
                    <li><a href="/">Home</a></li>
                    <li class="sep">/</li>
                    <li>Contact</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- content begin -->
<div id="content" class="no-top">
    <section class="no-top" aria-label="map-container">
        <div id="map"></div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <h3>Send Us Message</h3>
                <div id='mail_success' class='success'>Your message has been sent successfully.</div>
                <div id='mail_fail' class='error'>Sorry, error occured this time sending your message.</div>
                <form name="contactForm" id='contact_form'  action='{{route('contact.send')}}' method="post">
                    @csrf
                    <div class="row">

                        <div class="col-md-4">

                            <div id='name_error' class='error'>Please enter your name.</div>
                            <div>

                                <input type='text' name='name' id='name' class="form-control" placeholder="Your Name">
                            </div>

                            <div id='email_error' class='error'>Please enter your valid E-mail ID.</div>
                            <div>
                                <input type='text' name='email' id='email' class="form-control" placeholder="Your Email">
                            </div>

                            <div id='phone_error' class='error'>Please enter your phone number.</div>
                            <div>
                                <input type='text' name='phone' id='phone' class="form-control" placeholder="Your Phone">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div id='message_error' class='error'>Please enter your message.</div>
                            <div>
                                <textarea name='message' id='message' class="form-control" placeholder="Your Message"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
{{--                            <p id='submit'>--}}
{{--                                <input type='submit' id='send_message' value='Submit Form' class="btn btn-line">--}}
{{--                            </p>--}}
                            <button id='submit' type="submit" class="btn btn-line">Submit</button>

                        </div>
                    </div>
                </form>
            </div>

            <div id="sidebar" class="col-md-4">

                <div class="widget widget_text">
                    <h3>Contact Info</h3>
                    <address>
                        <span>{{$data['info']->company_address}}</span>
                        <span><strong>Phone:</strong>{{$data['info']->phone}}</span>
                        <span><strong>Email:</strong><a href="mailto:contact@example.com">{{$data['info']->email}}</a></span>
                        <span><strong>Web:</strong><a href="contact.html#test">{{$data['info']->web}}</a></span>
                        <span><strong>Open</strong>{{$data['info']->phone}}</span>
                    </address>
                </div>

            </div>
        </div>
    </div>
</div>


<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyAd1iF0qOMcbvehRChx4F1PxaB2MZPsUHc"></script>
<script src="{{asset('badger_assets/js/map.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script>
       $('#contact_form').on('submit',function(e){
           e.preventDefault();
           var url = $(this).attr('action');
           var name = $("#name").val();
           var email = $("#email").val();
           var phone = $("#phone").val();
           var message = $("#message").val();

           console.log(url);
           $.ajax({
               method: "POST",
               url: $(this).attr('action'),
               data:{
                      _token:'{{csrf_token()}}',
                      name:name,
                      email:email,
                      phone:phone,
                      message:message
               },
               success: function(data){
                    $('#mail_success').show();
                    setTimeout(()=>{
                        $('#mail_success').hide();
                   },5000);
                    console.log(data);
               },
               error:function (data) {
                    $('#mail_fail').show();
                    setTimeout(()=>{
                        $('#mail_fail').hide();
                    },5000);
               }
           })
       });
    </script>
@endsection