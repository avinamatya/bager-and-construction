@extends('layouts.layout')
@section('content')
    <!-- subheader -->
    <section id="subheader" data-speed="8" data-type="background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Cost</h1>
                    <ul class="crumb">
                        <li><a href="/">Home</a></li>
                        <li class="sep">/</li>
                        <li>Cost</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<div id="content">

    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <h2>Cost Calculation</h2>
                <form name="contactForm" id='contact_form'  action='' method="">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div id='name_error' class='error'>Please enter Anna </div>
                            <div>
                                <label for="">Anna</label>
                                <input type='text' name='anna' id='anna' class="form-control" placeholder="Anna">
                            </div>

                            <div id='email_error' class='error'>Please enter your valid E-mail ID.</div>
                            <div>
                                <label for="">Square Feet</label>
                                <input type='text' name='square' id='square' class="form-control" placeholder="Square Feet">
                            </div>

                            <div id='phone_error' class='error'>Please enter your phone number.</div>
                            <div>
                                <label for="">Total Cost</label>
                                <input type='text' name='phone' id='phone' class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            {{--                            <p id='submit'>--}}
                            {{--                                <input type='submit' id='send_message' value='Submit Form' class="btn btn-line">--}}
                            {{--                            </p>--}}
                            <button type="submit" id="submit" class="btn btn-line">Calculate</button>
                        </div>
                    </div>
                </form>
            </div>

            <div id="sidebar" class="col-md-4">

                <div class="widget widget_text">
                    <h3>Contact Info</h3>
                    <address>
                        <span>{{$data['info']->company_address}}</span>
                        <span><strong>Phone:</strong>{{$data['info']->phone}}</span>
{{--                        <span><strong>Fax:</strong>{{$data['info']->fax}}</span>--}}
                        <span><strong>Email:</strong><a href="mailto:contact@example.com">{{$data['info']->email}}</a></span>
                        <span><strong>Web:</strong><a href="contact.html#test">{{$data['info']->web}}</a></span>
                        <span><strong>Open</strong>{{$data['info']->phone}}</span>
                    </address>
                </div>

            </div>
        </div>
    </div>
</div>


</div>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script>
        $(document).ready(function () {
            $('#submit').click(function (event) {
                event.preventDefault();
                var anna = $('#anna').val();
                var square = $('#square').val();
                if(anna.length && square.length > 0){
                    var answer = parseInt(anna) + (parseInt(square)/342.25) * 100000;
                }
                $('#phone').val(answer.toFixed(2));

            });//end click
            $("#phone").prop("disabled", true);

            //Disable the submit button initially
            $("#submit").prop("disabled", true);
            //


            var annaSize = 0;
            var sqFeet = 0;
            var submitButton = $("#submit");
            //While the value is passed on anna
          $('#anna').on('keyup keypress blur', function(event){
              annaSize = $(this).val().length;
              submitButton.prop("disabled", true);
              if(sqFeet && annaSize){
                  submitButton.prop("disabled", false);
              }
              //this.value = this.value.replace(/[^0-9\.]/g,'');
              $(this).val($(this).val().replace(/[^0-9\.]/g,''));
              if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                  event.preventDefault();
              }
          });

          $('#square').on('keyup keypress blur', function(event){
              sqFeet = $(this).val().length;
              submitButton.prop("disabled", true);
              if(sqFeet && annaSize){
                  submitButton.prop("disabled", false);
              }
              //this.value = this.value.replace(/[^0-9\.]/g,'');
              $(this).val($(this).val().replace(/[^0-9\.]/g,''));
              if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                  event.preventDefault();
              }
          });

        });

    </script>
@endsection