<div class="container project-view">

    <div class="row">
        <div class="col-md-8 project-images" style="width: 60%">
            <img src="{{asset('storage/'.$team->image)}}" alt="" class="img-responsive" width="60%"/>
        </div>
        <div class="col-md-4">
            <div class="project-info">
                <h2>{{$team->name}}</h2>
                <div class="details">
                    <div class="info-text">
                        <span class="title">Post</span>
                        <span class="val">{{$team->post}}</span>
                    </div>

                    <div class="info-text">
                        <span class="title">Email</span>

                        <span class="val">{{$team->email}}</span>
                    </div>

                    <div class="info-text">
                        <span class="title">Phone</span>
                        <span class="val">{{$team->phone}}</span>
                    </div>

                </div>
                <p>{{$team->description}}</p>

{{--                <p>--}}
{{--                <h4>Our Solutions</h4>--}}
{{--                {{$projectdetail->solution}}--}}
{{--                </p>--}}
{{--                </p>--}}


            </div>
        </div>
    </div>
</div>