@extends('layouts.layout')
@section('content')
    <!-- subheader -->
{{--    {{dd(asset('storage/images/'.$data['projectbanner'][0]->image))}}--}}
    <section id="subheader" style="background: url({{asset('storage/'.$data['projectbanner'][0]->image)}})" data-speed="8" data-type="background" class="text-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Projects</h1>
                    <ul class="crumb">
                        <li><a href="/">Home</a></li>
                        <li class="sep">/</li>
                        <li>Projects</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div id="content">
        <div class="container">

            <div class="spacer-single"></div>

            <!-- portfolio filter begin -->
            <div class="row">
                <div class="col-md-12">

                    <ul id="filters" class="wow fadeInUp" data-wow-delay="0s">
                        @foreach($data['projectlist'] as $list)
                            <li><a href="project-contained-4-cols.html#"
                                   data-filter=".{{$list->title}}">{{$list->title}}</a></li>
                            @if($loop->last)
                                <li class="pull-right">
                                    <a href="project-contained-4-cols.html#" data-filter="*"
                                       class="selected">All Projects</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>

                </div>
            </div>
            <!-- portfolio filter close -->

            <div id="gallery" class="row gallery full-gallery de-gallery pf_4_cols wow fadeInUp" data-wow-delay=".3s">

                <!-- gallery item -->


                @foreach($data['projects'] as $project)

                    <div class="col-md-3 col-sm-6 col-xs-12 item mb30 {{ $project->projectlist->title }}">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="{{route('project_details',$project->id)}}">
                        <span class="overlay">
                            <span class="pf_text">
                                <span class="project-name">{{$project->title}}</span>
                            </span>
                        </span>
                            </a>
                            <img src="{{asset('storage/'.$project->image1)}}" alt=""/>
                        </div>
                    </div>
            @endforeach

            <!-- close gallery item -->

                <!-- close gallery item -->

            </div>
        </div>
    </div>
    <section id="view-all-projects" class="call-to-action bg-color text-center" data-speed="5" data-type="background"
             aria-label="view-all-projects">
        <a href="contact" class="btn btn-line-black btn-big">Get Quotation</a>
    </section>

@endsection