<div class="container project-view">

	<div class="row">
        <div class="col-md-8 project-images">
            @if($projectdetail->url == null)
            <img src="{{asset('storage/'.$projectdetail->image1)}}" alt="" class="img-responsive" />
            <img src="{{asset('storage/'.$projectdetail->image2)}}" alt="" class="img-responsive" />
            <img src="{{asset('storage/'.$projectdetail->image3)}}" alt="" class="img-responsive" />
            @else
            <iframe width="100%" height="500px" src="{{ $projectdetail->url }}" frameborder="0" allowfullscreen>
            </iframe>
            <img src="{{asset('storage/'.$projectdetail->image1)}}" alt="" class="img-responsive" />
            <img src="{{asset('storage/'.$projectdetail->image2)}}" alt="" class="img-responsive" />
            <img src="{{asset('storage/'.$projectdetail->image3)}}" alt="" class="img-responsive" />
            @endif
        </div>
        <div class="col-md-4">
            <div class="project-info">
                <h2>{{$projectdetail->title}}</h2>
                <div class="details">
                    <div class="info-text">
                        <span class="title">Date</span>
                        <span class="val">{{$projectdetail->formatted_date}}</span>
                    </div>

                    <div class="info-text">
                        <span class="title">Location</span>

                        <span class="val">{{$projectdetail->location}}</span>
                    </div>

                    <div class="info-text">
                        <span class="title">Client</span>
                        <span class="val">{{$projectdetail->client}}</span>
                    </div>

                    <div class="info-text">
                        <span class="title">Category</span>
                        <span class="val">{{$projectdetail->projectlist['title']}}</span>
                    </div>
                </div>
                <h4>Description</h4>
                <p>{{$projectdetail->description}}</p>
            </div>
        </div>
    </div>
</div>