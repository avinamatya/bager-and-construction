@extends('layouts.layout')
@section('content')
<!-- subheader -->
<section id="subheader" data-speed="8" data-type="background">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>{{$data['services']->service_title}} {{$data['services']->title}}</h1>
                <ul class="crumb">
                    <li><a href="/">Home</a></li>
                    <li class="sep">/</li>
                    <li><a href="{{ route('services')}}">Services</a></li>
                    <li class="sep">/</li>
                    <li>{{$data['services']->service_title}} {{$data['services']->title}}</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- subheader close -->

<!-- content begin -->
<div id="content">
    <div class="container">
        <div class="row">


            <div id="sidebar" class="col-md-3 wow fadeInUp">
                <ul id="services-list">
                @foreach($data['allServices'] as $service)
                    <li @if($service->id == $id) class="active" @endif >
                        <a href="{{route('single.service',$service->id)}}">{{$service->service_title}} {{$service->title}}</a>
                    </li>
                        @foreach($service->servicetype as $my)
                        <li class="inside-list">
                            <a href="{{route('single.service-type',[$service->id,$my->id])}}" >{{$my->title}}</a></li>
                        @endforeach

                    @endforeach

                </ul>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 wow fadeInUp" data-wow-delay=".3s">

                            <p>{{$data['services']->service_desc}}</p>

                    </div>
                    <div class="col-md-6 pic-services wow fadeInUp" data-wow-delay=".6s">
                        <img src="{{asset('storage/'.($data['services']->image1))}}" class="img-responsive" alt="">
                        <img src="{{asset('storage/'.($data['services']->image2))}}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>


        </div>
    </div>



</div>

<section id="view-all-projects" class="call-to-action bg-color text-center" data-speed="5" data-type="background" aria-label="view-all-projects">
    <a href="contact" class="btn btn-line-black btn-big">Get Quotation</a>
</section>

@endsection