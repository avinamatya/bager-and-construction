@extends('layouts.layout')
@section('content')
<div id="content" class="no-bottom no-top">
{{--    {{dd(asset('/storage/'.$data['hire'][0]->image))}}--}}
<!-- revolution slider begin -->
<section id="section-welcome-13" style="background: url({{asset('/storage/'.$data['hire'][0]->image) }}) bottom right fixed" data-speed="3" data-type="background">

    <div class="center-y">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="spacer-double"></div>
                    @foreach($data['hire'] as $hire)
                        <h2 class="style-3">{{$hire->title1}}<strong class="id-color">&amp;</strong> {{$hire->title2}}</h2>
                        <div class="spacer-half"></div>
                        <p class="lead">{{$hire->description}} </p>
                    @endforeach
                    <div class="spacer-single"></div>
                    <a href="contact" class="btn btn-line-black btn-big scroll-to">Hire Us Now</a>
                    <div class="spacer-double"></div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- revolution slider close -->

<!-- section begin -->
<section id="section-no-bg">
    <div class="container">
        <div class="row table">

            <div class="col-md-6 table-cell">
                <img src="{{asset('storage/'.$data['about'][0]->image)}}" alt="" class="img-responsive wow fadeInUp" data-wow-duration="1.5s">
            </div>

            <div class="col-md-6 table-cell wow fadeInUp">
                <h2>{{$data['about'][0]->title}}</h2>

                <p class="lead">{{$data['about'][0]->heading}}</p>

                <p>{{\Illuminate\Support\Str::limit($data['about'][0]->description,100) }}</p>
                <a href="about" class="btn-custom">Read more</a>
                <div class="spacer-single"></div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</section>
<!-- section close -->

<!-- section begin -->
<section id="section-services" class="no-top no-bottom">
    <div class="container">
        <div class="row">
            @foreach($data["services"]->slice(0,3) as $service)
            <div class="col-md-4 text-middle text-light wow fadeInRight" data-wow-delay="0">
                <div class="shadow-soft" data-bgimage="url(images/services/t1.jpg)">
                    <div class="padding40 overlay40">
                        <h3>{{$service->service_title}}</h3>
                        <p>{{Illuminate\Support\Str::limit($service->service_desc,250)}}</p>
                        <a href="{{route('single.service',$service->id)}}" class="btn-line btn-fullwidth">Service Details</a>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>
<!-- section close -->

<section id="section-why-choose-us-2">

    <div class="container">
        <div class="row">

            <div class="row">
                @foreach($data['extra'] as $extra)
                <div class="col-md-4 wow fadeInUp" data-wow-delay="{{$extra->delay1}}">
                    <div class="box-icon">
                        <span class="icon wow fadeIn" data-wow-delay="{{$extra->delay2}}"><i class="id-color icon-{{$extra->icon}}"></i></span>
                        <div class="text">
                            <h3>{{$extra->title}}</h3>
                            <p>{{$extra->description}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>
<!-- section close -->
    <section id="" style="background: url({{asset('/storage/'.$data['testinomal'][0]->image)}})" data-speed="3" data-type="background" class="text-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp">
                    <h1>{{$data['testinomal'][0]->title}}</h1>
                    <div class="separator"><span><i class="fa fa-circle"></i></span></div>
                    <div class="spacer-single"></div>
                </div>
            </div>
            <div id="testimonial-carousel" class="de_carousel  wow fadeInUp" data-wow-delay=".3s">
                @foreach($data['customer'] as $customer)
                    <div class="col-md-6 item">
                        <div class="de_testi">
                            <blockquote>
                                <p>{{$customer->description}}</p>
                                <div class="de_testi_by">
                                    {{$customer->name}}, Customer
                                </div>
                            </blockquote>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
<!-- section begin -->
<section id="view-all-projects" class="call-to-action text-light padding20" data-speed="5" data-type="background" aria-label="cta" data-bgcolor="#3562a8">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Are you ready to makeover your home? </h2>
            </div>

            <div class="col-md-4 text-right">
                <a href="contact" class="btn btn-line btn-big mt20 wow fadeInUp">Get Quotation</a>
            </div>
        </div>
    </div>
</section>
<!-- logo carousel section close -->
</div>
@endsection