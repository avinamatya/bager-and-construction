@extends('layouts.layout')
@section('content')
<!-- subheader -->
}
<section id="subheader" style="background: url({{asset('storage/'.($data['servicebanner'][0]->image))}}" data-speed="8" data-type="background">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>{{$data['servicebanner'][0]->title}}</h1>
                <ul class="crumb">
                    <li><a href="/">Home</a></li>
                    <li class="sep">/</li>
                    <li>Services</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- subheader close -->
<div id="content">
    <div class="container">
        <div class="row">
            @foreach($data['services'] as $service)

            <div class="col-md-3">
                <h3><span class="id-color">{{$service->service_title}}</span> {{$service->title}}</h3>
                    {{Illuminate\Support\Str::limit($service->service_desc,250)}}
                <div class="spacer-single"></div>
                <img src="{{asset('storage/'.$service->image1)}}" class="img-responsive" alt="">
                <div class="spacer-single"></div>
                <a href="{{route('single.service',$service->id)}}" class="btn-line btn-fullwidth">Read More</a>
            </div>
                @endforeach

        </div>
    </div>



</div>
<section id="view-all-projects" class="call-to-action bg-color text-center" data-speed="5" data-type="background" aria-label="view-all-projects">
    <a href="contact" class="btn btn-line-black btn-big">Get Quotation</a>
</section>

@endsection