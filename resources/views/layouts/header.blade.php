<header class="header-light relative">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- logo begin -->
                <div id="logo">
                    <a href="/">
                    <img class="logo" src="/storage/{{$data['info']->company_logo}}" style="width:150px" alt="">
                    </a>
                </div>
                <!-- logo close -->

                <!-- small button begin -->
                <span id="menu-btn"></span>
                <!-- small button close -->

                <!-- mainmenu begin -->
                <nav>
                    <ul id="mainmenu">
                        <li><a href="{{route('index')}}">Home</a></li>
                        <li><a href="{{route('about')}}">About Us</a></li>
                        <li><a href="{{route('projects')}}">Projects</a> </li>
                        <li><a href="{{route('services')}}">Services</a></li>
                        <li><a href="{{route('cost')}}">Cost Calculator</a></li>
                        <li><a href="{{route('contact')}}">Contact Us</a></li>
                    </ul>
                </nav>

            </div>
            <!-- mainmenu close -->

        </div>
    </div>
</header>