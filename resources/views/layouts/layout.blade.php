<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Badger Construction</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Badger Construction Pvt Ltd">
    <meta name="keywords" content="architecture,building,business,bootstrap,creative,exterior design,furniture design,gallery,garden design,house,interior design,landscape design,multipurpose,onepage,portfolio,studio">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/png" href="/storage/{{$data['info']->favicon}}" />
    <link rel="shortcut icon" type="image/png" href="/storage/{{$data['info']->favicon}}" />

    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="{{asset('badger_assets/css/bootstrap.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/jpreloader.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/animate.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/plugin.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/owl.theme.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/owl.transitions.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/demo/demo.css')}}" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="{{asset('badger_assets/css/bg.css')}}" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="{{asset('badger_assets/css/color.css')}}" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="{{asset('badger_assets/fonts/font-awesome/css/font-awesome.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/fonts/elegant_font/HTML_CSS/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/fonts/et-line-font/style.css')}}" type="text/css">

    <!-- revolution slider -->
    <link rel="stylesheet" href="{{asset('badger_assets/rs-plugin/css/settings.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('badger_assets/css/rev-settings.css')}}" type="text/css">
</head>

<body id="homepage">

    <div id="wrapper">

        <!-- header begin -->
        @include('layouts.header')
        <!-- header close -->
        <!-- content begin -->
        @yield('content')
        <!-- footer begin -->
        @include('layouts.footer')
        <!-- footer close -->
    </div>
    </div>
    <!-- Javascript Files
    ================================================== -->
    <script src="{{asset('badger_assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('badger_assets/js/jpreLoader.js')}}"></script>
    <script src="{{asset('badger_assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('badger_assets/js/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('badger_assets/js/easing.js')}}"></script>
    <script src="{{asset('badger_assets/js/jquery.flexslider-min.js')}}"></script>
    <script src="{{asset('badger_assets/js/jquery.scrollto.js')}}"></script>
    <script src="{{asset('badger_assets/js/owl.carousel.js')}}"></script>
    <script src="{{asset('badger_assets/js/jquery.countTo.js')}}"></script>
    <script src="{{asset('badger_assets/js/classie.js')}}"></script>
    <script src="{{asset('badger_assets/js/video.resize.js')}}"></script>
    <script src="{{asset('badger_assets/js/validation.js')}}"></script>
    <script src="{{asset('badger_assets/js/wow.min.js')}}"></script>
    <script src="{{asset('badger_assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('badger_assets/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('badger_assets/js/enquire.min.js')}}"></script>
    <script src="{{asset('badger_assets/js/designesia.js')}}"></script>
    <script src="{{asset('badger_assets/demo/demo.js')}}"></script>


    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="{{asset('badger_assets/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('badger_assets/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>



</body>

</html>