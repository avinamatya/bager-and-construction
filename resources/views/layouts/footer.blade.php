<footer class="light">
    <div class="container pb40">
        <div class="row">
            <div class="col-md-3">
                <img src="storage/{{$data['info']->company_logo}}" style="width:150px" class="logo-small" alt=""><br>
            </div>

            <div class="col-md-6">
                &copy; Copyright {{now()->format("Y")}} - Badger Construction Pvt. Ltd.. Powered by <a href="http://silkinnovation.com.np" target="_blank"><span class="id-color">Silk Innovation Pvt. Ltd.</span></a>
            </div>

            <div class="col-md-3 text-right">
                <div class="social-icons">
                    <a href="{{$data['info']->facebook}}" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
                    <a href="{{$data['info']->instagram}}" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>
                </div>
            </div>
        </div>
    </div>


    <a href="index-arts.html#" id="back-to-top"></a>
</footer>