@extends('layouts.layout')
@section('content')
<!-- subheader -->
<section id="subheader" style="background: url({{asset('/storage/'.$data['aboutbanner'][0]->image) }}) " data-speed="8" data-type="background" class="text-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>{{$data['aboutbanner'][0]->title}}</h1>
                <ul class="crumb">
                    <li><a href="/">Home</a></li>
                    <li class="sep">/</li>
                    <li>About Us</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- subheader close -->
<!-- content begin -->
<div id="content" class="no-top no-bottom">

    @foreach($data['about'] as $about)

    <section id="section-about-us-2"  class="side-bg no-padding">

        <div class="image-container col-md-5  pull-right " style="background: url({{asset("/storage/".$about->image)}});@if(($loop->index + 1 ) %2==0) right:0 @endif " data-delay="0">

        </div>

        <div class="container">
            <div class="row">
                <div class="inner-padding">
                    <div class="col-md-6 @if(($loop->index + 1 ) %2 != 0) col-md-offset-6 @endif wow fadeInRight" data-wow-delay=".5s">

                        <h2>{{$about->title}}</h2>

                        <p class="intro">{{$about->heading}}</p>

                         <p>{{$about->description}}</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
@endforeach
<!--team -->

    <section id="section-team">
        <div class="container">
            <div class="row">
                <div class="col-md-12 container-4">
                    @foreach($data['team'] as $team)
                    <!-- team member -->
                    <div class="de-team-list">
                        <div class="team-pic">

                            <img src="/storage/{{$team->image}}" class="img-responsive" alt="" />

                        </div>
                        <div class="team-desc col-md-12">
                            <a class="simple-ajax-popup-align-top" href="{{route('singleteam',$team->id)}}" style="text-decoration : none">
                            <h3>{{$team->name}}</h3>

                            <p class="lead">{{$team->post}}</p>

                            <div class="small-border"></div>
                            <p>{{$team->shortdescription}}</p>

                            <div class="social">
                                <a href="#"><i class="fa fa-phone fa-lg"></i>&nbsp;</a>{{$team->phone}}
                                <br/>
                                <a href="mailto:{{$team->email}}"><i class="fa fa-envelope fa-lg"></i>&nbsp;</a>{{$team->email}}
                                <br>
                            </div>
                                <div class="social text-center" style="margin-top: 10%;">
                                    @if($team->facebook)
                                    <a href="{{$team->facebook}}"  target="_blank"><i class="fa fa-facebook fa-lg" style="padding: 10px"></i></a>
                                    @endif
{{--                                    <a href="www."><i class="fa fa-google-plus fa-lg" style="padding: 10px"></i></a>--}}
                                        @if($team->instagram)
                                    <a href="https://www.instagram.com" target="_blank"><i class="fa fa-instagram fa-lg" style="padding: 10px"></i></a>
                                        @endif
{{--                                    <a href="index-arts.html#"><i class="fa fa-twitter fa-lg" style="padding: 10px"></i></a>--}}
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- team close -->
                    @endforeach
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </section>



    <!-- section begin -->
    <section id="view-all-projects" class="call-to-action bg-color dark text-center" data-speed="5" data-type="background" aria-label="view-all-projects">
        <a href="contact" class="btn btn-line-black btn-big">Talk With Us</a>
    </section>
    <!-- logo carousel section close -->
</div>

@endsection